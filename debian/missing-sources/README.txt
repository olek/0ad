This directory provides the non-minified versions of the embedded JQuery files
in the orig source tarball, in order to comply with Debian Policy. (#735349)

The minified javascript files for which this directory provides the
non-minified versions are as follows. Each of the lines below indicate the
minified file, followed by the non-minified source file(s) present in this
directory.

source/tools/replayprofile/jquery.flot.js:
    jquery.colorhelpers.js
source/tools/replayprofile/jquery.flot.navigate.js:
    jquery.event.drag.js
    jquery.mousewheel.js

source/tools/templatesanalyzer/tablefilter/tablefilter.js:
source/tools/templatesanalyzer/tablefilter/tf-1.js:
    tablefilter/*.js

(Tablefilter 0.0.11: https://github.com/koalyptus/TableFilter/tree/4f2316a4b57021a41edd10d3b98e7f8159642392)


